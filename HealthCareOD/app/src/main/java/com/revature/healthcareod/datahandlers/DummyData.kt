package com.revature.healthcareod.datahandlers

import Models.Job

object DummyData {
    val dummyJH=ArrayList<Job>()
    init{
        var temp =Job()
        temp.jobDescription="Nursing"
        temp.hospitalId=4
        temp.ratePerHour=15
        temp.totalAmount=120
        temp.status= "Open"
        temp.jobName="Emergency Nurse Opening"
        dummyJH.add(temp)

        temp =Job()
        temp.jobDescription="Nursing"
        temp.hospitalId=5
        temp.ratePerHour=25
        temp.totalAmount=300
        temp.status= "Open"
        temp.jobName="Emergency Nurse Opening"
        dummyJH.add(temp)

        temp =Job()
        temp.jobDescription="Nursing"
        temp.hospitalId=5
        temp.ratePerHour=25
        temp.totalAmount=250
        temp.status= "Open"
        temp.jobName="Emergency Nurse Opening"
        dummyJH.add(temp)

        temp =Job()
        temp.jobDescription="Nursing"
        temp.hospitalId=5
        temp.ratePerHour=22
        temp.totalAmount=220
        temp.status= "Accepted"
        temp.jobName="Emergency Nurse Opening"
        dummyJH.add(temp)

    }
    fun getHosNameBId(int:Int):String{
        return when(int){
            1->"Hochkis Hospital"
            2->"John's Hospital"
            3->"Hospital of Great Mercy"
            4->"Nurse Ratchet's Hospital"
            5->"Good Brothers Hospital"
            else->"Unknown Hospital"
        }
    }

}