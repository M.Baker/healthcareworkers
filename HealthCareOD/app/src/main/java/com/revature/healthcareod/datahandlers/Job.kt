package Models

import java.time.LocalDateTime

enum class status {
    Open, Accepted, Declined, Completed
}

class Job {
    var jobId = 0
    var hospitalId = 0
    var userId = ""
    var ratePerHour = 0
    var totalAmount = 0
    var jobName: String? = null
    var jobDescription: String? = null
    var startDate: LocalDateTime? = null
    var endDate: LocalDateTime? = null
    var status: String? = null
        set(status) {
            val status = Models.status.valueOf(status!!)
            field = status.name
        }
}