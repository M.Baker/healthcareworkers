package com.revature.healthcareod.screens

import android.os.Bundle
import android.text.SpannableStringBuilder
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.R
import com.revature.healthcareod.datahandlers.NurseAccount
import com.revature.healthcareod.datahandlers.Users
import com.revature.healthcareod.databinding.FragmentAccountViewBinding
import com.revature.healthcareod.datahandlers.Degree
import com.revature.healthcareod.datahandlers.EduAdapter

// Account view by Matt
class AccountViewFragment : Fragment() {

    private var _binding: FragmentAccountViewBinding? = null
    private val binding get() = _binding!!
    lateinit var userName: String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Load the user

        arguments?.let {
            userName = it.getString("user").toString()
        }
        if (userName != null) {
            userRef = Users.getUser(userName)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAccountViewBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.accViewRecV.adapter= EduAdapter(this.requireContext(),userRef!!.education)
        binding.btnBack.setOnClickListener { back() }
        binding.btnSaveProfile.setOnClickListener { submit() }
        binding.btnSavePassword.setOnClickListener { savePass() }
        binding.btnSubmitEdu.setOnClickListener { saveEdu() }
        binding.editTextAccViewFName.text=SpannableStringBuilder(userRef?.firstName ?:"")
        binding.editTextAccViewLName.text=SpannableStringBuilder(userRef?.lastName ?:"")
        binding.editTextAccViewAdrs.text=SpannableStringBuilder(userRef?.address ?:"")
        //Inflate the layout for this fragment
        return view
    }

    // make a new item for the list of education items

    private fun saveEdu() {
        val s =binding.accViewSchoolEt.text.toString()
        val d=binding.accViewGradDat.text.toString()
        val dg=binding.accViewLevel.text.toString()
        val m=binding.accViewMajorEt.text.toString()
        if(s.trim()!=""&&d.trim()!=""&&dg.trim()!=""&&m.trim()!=""){
            userRef?.let{
                it.education.add(Degree(dg,s,d,m))
                binding.accViewRecV.adapter?.notifyDataSetChanged()
            }
        }
    }

    private fun back() {
        findNavController().navigate(
                AccountViewFragmentDirections.actionAccountViewFragmentToJobSearcher(
                        user = userName
                )
        )
    }

    //update misc profile info

    private fun submit() {
        userRef?.let {
            val fName = binding.editTextAccViewFName.text.toString().trim()
            val lName = binding.editTextAccViewLName.text.toString().trim()
            val adrs = binding.editTextAccViewAdrs.text.toString()
            var scs=false
            if (fName.trim() != "" && lName.trim() != "" && fName != it.firstName && lName != it.lastName) {
                it.firstName = fName.trim()
                it.lastName = lName
                binding.editTextAccViewFName.text=SpannableStringBuilder(fName)
                binding.editTextAccViewLName.text=SpannableStringBuilder(lName)
                scs=true
            }
            if (adrs.trim() != "" && adrs != it.address) {
                it.address = adrs
                binding.editTextAccViewAdrs.text=SpannableStringBuilder(adrs)
                scs=true
            }
            if (scs){
                Toast.makeText(requireContext(), getString(R.string.success_profile), Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(requireContext(), getString(R.string.error_message_nochanges), Toast.LENGTH_SHORT).show()
            }
        }
    }

    /// change whats in passwork if it meets password requirements

    private fun savePass() {
        userRef?.let {
            val p = binding.editTextPassword.text.toString()
            val p2 = binding.editTextTextConfirmPassword.text.toString()
            if (p != "") {
                if (p == p2) {
                    it.password = p
                    Toast.makeText(requireContext(), getString(R.string.success_password), Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(requireContext(), getString(R.string.error_message_unmatched), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(requireContext(), getString(R.string.blank_password), Toast.LENGTH_SHORT).show()
            }
        }

    }


}