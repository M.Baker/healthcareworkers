package com.revature.healthcareod.datahandlers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R

class EduAdapter(private val context:Context, private val dataset:List<Degree>) : RecyclerView.Adapter<EduAdapter.ItemViewHolder>() {

    class ItemViewHolder(private val view : View):RecyclerView.ViewHolder(view){
        val s: TextView =view.findViewById(R.id.rv_school_tv)
        val d: TextView =view.findViewById(R.id.rv_date_tv)
        val dg: TextView =view.findViewById(R.id.rv_level_tv)
        val m: TextView =view.findViewById(R.id.rv_major_tv)

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.edu_element,parent,false)
        return ItemViewHolder(adapterLayout)
    }


    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val degree=dataset[position]
        holder.s.text=degree.school
        holder.d.text=degree.gradDate
        holder.dg.text=degree.level
        holder.m.text=degree.major
    }

}
