/*
Healthcare OnDemand
Anderson Adams
Login UI Class
Class Login UI will push to registration or check if user can login
 */

package com.revature.healthcareod.screens

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.*
import com.revature.healthcareod.datahandlers.NurseAccount
import com.revature.healthcareod.datahandlers.Users


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tempUser = Users()//single instance of users class to access nurse list

        //User input taken from firstFragment.xml
        val userName = view.findViewById<EditText>(R.id.FirstFragmentTextUserName)
        val password = view.findViewById<EditText>(R.id.FirstFragmentTextPassword)

        //Registration Button to Registration Fragment
        view.findViewById<Button>(R.id.button_first).setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)//Send user to registration
        }//registration button listener


        //Login Button to Customer Fragment
        view.findViewById<Button>(R.id.buttonLogin).setOnClickListener{
           val tempAccount: NurseAccount? = tempUser.login(userName.text.toString(), password.text.toString())

            /*
            First meathod using an import of compainion object
            if(customers[userName.text.toString()] != null && customers[userName.text.toString()]!!.password == password.text.toString()){ //employee password matches
                println("Found account in log in ")
                findNavController().navigate(R.id.action_FirstFragment_to_jobSearcher)
                 return customers[userName]
            } else{
                println("Could not find account login")
                startActivity( Intent(this.requireContext(), MainActivity::class.java) )
            }
            println("Nurse Account login attempt made")
            if (account != null) {
                println("account ${account.userName}")
            }
           */

            //check if user logged in, restart activity
            if(tempAccount == null){
                println("Can not find customer account")
                startActivity( Intent(this.requireContext(), MainActivity::class.java) )
            }
            else{//user logged in, pass to main customer IO fragment

                findNavController().navigate(
                    FirstFragmentDirections.actionFirstFragmentToJobSearcher(
                        user = tempAccount.userName
                    )
                )
           }//else
        }//login button listener

    }//function on view created

}//class First Fragment