package com.revature.healthcareod.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.databinding.FragmentScheduleBinding
import com.revature.healthcareod.datahandlers.NurseAccount
import com.revature.healthcareod.datahandlers.Users

class ScheduleFragment : Fragment() {

    private var _binding: FragmentScheduleBinding?=null
    private val binding get()=_binding!!
    lateinit var userName:String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let{
            userName=it.getString("user").toString()
        }
        if(userName!=null){
            userRef= Users.getUser(userName)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding= FragmentScheduleBinding.inflate(inflater,container,false)
        val view=binding.root


        // binding.btnBack.setOnClickListener { back() }
        //binding.btnSaveProfile.setOnClickListener { submit() }
        //Inflate the layout for this fragment
        return view
    }

    private fun back(){
        findNavController().navigate(
            ScheduleFragmentDirections.actionScheduleToJobSearcher(
                user = userName
            )
        )
    }
}