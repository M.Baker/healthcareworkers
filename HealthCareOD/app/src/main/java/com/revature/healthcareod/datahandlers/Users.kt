/*
Healthcare OnDemand
Anderson Adams
Users Class
Class will hold main list of customers, transactions, and registration
 */

package com.revature.healthcareod.datahandlers

import android.content.Context
import android.widget.Toast
import com.revature.healthcareod.MainActivity
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.*

open class Users {

    private val file: String = "myUsers.txt"
    //static object holding customer accounts and transactions
    companion object UsersLists{
        var customers : HashMap<String, NurseAccount> = HashMap<String, NurseAccount> ()
        var transactions : ArrayList<String> = ArrayList<String>()
        fun getUser(userName: String): NurseAccount?{
            return customers[userName]
        }
    }

    //registration function will make sure first name, last, user, and password are
    fun register(firstName: String, lastName: String, userName: String, password: String, phone: String, address: String, context: Context){

        if(customers.containsKey(userName)){
            val toast = Toast.makeText(context, "Account already registered", Toast.LENGTH_SHORT)
            toast.show()
            return
        }//if account exists

        if(firstName.isEmpty() || firstName == "null") {
            val toast = Toast.makeText(context, "Incorrect First name", Toast.LENGTH_SHORT)
            toast.show()
            return
        }//if first name is correctly entered
        if(lastName.isEmpty() || lastName == "null") {
            val toast = Toast.makeText(context, "Incorrect Last name", Toast.LENGTH_SHORT)
            toast.show()
            return
        }//if last name is correctly entered
        if(userName.isEmpty() || userName == "null") {
            val toast = Toast.makeText(context, "Incorrect User name", Toast.LENGTH_SHORT)
            toast.show()
            return
        }//if user name is correctly entered
        if(password.isEmpty() || password == "null") {
            val toast = Toast.makeText(context, "Incorrect password", Toast.LENGTH_SHORT)
            toast.show()
            return
        }//if password is correctly entered

        val tempUser = NurseAccount(firstName, lastName, userName, password, phone, address) //create temp Nurse account
        customers[userName] = tempUser //create user

        transactions.add("Registered Account: ${tempUser.firstName}, Date: ")

        val toast = Toast.makeText(context, "Account registered, proceeded to login.", Toast.LENGTH_SHORT)
        toast.show()
    }//function register new nurse

    fun login(userName: String, password: String): NurseAccount?{
        return if(customers[userName] != null && customers[userName]!!.password == password){ //if user exists and the password entered matches
            customers[userName]

        } else{
            null
        }
    }//function login


    fun verifyUser(account: NurseAccount){
        account.verify()
    }//fun verify users maybe to take out


    fun readUsers(mainActivity: MainActivity) { //currently not working
        var input: String
        try {
            mainActivity.baseContext.openFileInput(file).use { stream ->
                input = stream.bufferedReader().use { it.readText() }
            }

            if(input.isNotEmpty()){
                val readL = Json.decodeFromString<FileHandle>(input)
                customers = readL.customers
            }
            println("customers $customers")

        } catch (e: FileNotFoundException) {
            println("file not found")
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            println("number format")
            e.printStackTrace()
        } catch (e: IOException) {
            println("IO exception")
            e.printStackTrace()
        }

    }//read users to json object, currently unfunctional

    fun writeUsers(mainActivity: MainActivity) {
        try {
            val content = Json.encodeToString(FileHandle(customers, transactions))
            mainActivity.baseContext.openFileOutput(file, Context.MODE_PRIVATE).use {
                output -> output.write(content.toByteArray())
            }

        }catch(e: Exception){
            println("=== Could not write the users lists ===")
            e.printStackTrace()
        }

    }//write users to json object, currently unfunctional

}