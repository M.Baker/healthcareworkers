package com.revature.healthcareod.datahandlers

import Models.Job
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R

class JobAdapter(private val context:Context,private val dataset:List<Job>):RecyclerView.Adapter<JobAdapter.ItemViewHolder>() {
    class ItemViewHolder(view: View):RecyclerView.ViewHolder(view){
        val h: TextView =view.findViewById(R.id.jh_hospital_name_tv)
        val jn:TextView=view.findViewById(R.id.jh_job_name_tv)
        val d:TextView=view.findViewById(R.id.jh_description_tv)
        val r:TextView=view.findViewById(R.id.jh_rate_tv)
        val t:TextView=view.findViewById(R.id.jh_total_tv)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.job_element,parent,false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val job=dataset[position]
        holder.h.text=DummyData.getHosNameBId(job.hospitalId)
        holder.jn.text=job.jobName
        holder.d.text=job.jobDescription
        holder.r.text=job.ratePerHour.toString()
        holder.t.text=job.totalAmount.toString()
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

}