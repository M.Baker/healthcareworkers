# Health Care Workers

## This mock app aims to make it easier to find short notice licensed health care workers

## Technologies used:
* Android SDK 30
* Kotlin 1.4

## Features:
* Register Account
* Login
* Search for jobs
* Accept jobs
* View and edit account details
* View history of jobs

## Getting Started:
* Compile and run the app

## Usage:
* Go the registration screen to register an account
* Login
* Begin choosing jobs

## Contributors:
* Uladzislau Shoka
* Anderson Adams
* Matthew Baker
