/*
Healthcare OnDemand
Anderson Adams
Job finder Class
Class will create individual views for recycler view. Hold all job info and accept button
 */
package com.revature.healthcareod.datahandlers

import Models.Job
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R

class JobFinder(private val context: Context, private val dataset: ArrayList<Job>, userName: String):RecyclerView.Adapter<JobFinder.ItemViewHolder>()  {
    private val tempUser = Users.getUser(userName)//temp user

    //holder text views and accept button
    class ItemViewHolder(view: View): RecyclerView.ViewHolder(view){
        val h: TextView =view.findViewById(R.id.jh_hospital_name_tv)
        val jn: TextView =view.findViewById(R.id.jh_job_name_tv)
        val d: TextView =view.findViewById(R.id.jh_description_tv)
        val r: TextView =view.findViewById(R.id.jh_rate_tv)
        val t: TextView =view.findViewById(R.id.jh_total_tv)
        val accept: Button = view.findViewById(R.id.jh_accept_button)

    }

    //inflate adapter holding job finder views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.job_finder_element,parent,false)
        return ItemViewHolder(adapterLayout)
    }

    //set all text data and listen for accept button at each job in list
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val job=dataset[position]
        holder.h.text=DummyData.getHosNameBId(job.hospitalId)
        holder.jn.text=job.jobName
        holder.d.text=job.jobDescription
        holder.r.text=job.ratePerHour.toString()
        holder.t.text=job.totalAmount.toString()
        holder.accept.setOnClickListener{
            if (tempUser != null) {
                tempUser.addJob(job)
                dataset[position].userId = tempUser.userName
            }
            dataset[position].status = "Accepted"
            dataset.remove(dataset[position])
            notifyDataSetChanged()
        }

    }

    override fun getItemCount(): Int {
        return dataset.size
    }

}