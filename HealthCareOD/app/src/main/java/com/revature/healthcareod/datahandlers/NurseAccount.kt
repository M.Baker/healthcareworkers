/*
Healthcare OnDemand
Anderson Adams
Nurse Account Class
Class will be object reference to nurse account that will hold names, ids, and job history
 */

package com.revature.healthcareod.datahandlers

import Models.Job
import java.io.Serializable

class NurseAccount(var firstName: String, var lastName: String, val userName: String, var password: String, var phone: String, var address: String) : Serializable {
    //nurse account info, made serializable to be sent through intents.

    val education=ArrayList<Degree>()
    //add degree info here

    val jobHistory = ArrayList<Job>()
    //job history

    private var verified: Boolean = false

    fun getJobHistory():List<Job>{
        return jobHistory
    }//will return all jobs that customer has accepted

    fun getAllOpenJobs():ArrayList<Job>{
        return DummyData.dummyJH.filter { it.status == "Open" } as ArrayList<Job>
    }//will return all jobs in dummy list of jobs

    fun getAllAcceptedJobs():ArrayList<Job>{
        return DummyData.dummyJH.filter { it.userId == userName } as ArrayList<Job>
    }//returns list of all josb that customer has accepted, differs from job history as jobs can be rejected here and does not include duplicates

    fun addJob(job: Job){
        jobHistory.add(job)
    }//adds job to job history, does include duplicates

    fun view_Account(){
        println("Account Id: $userName")
        println("Password: $password")
        println("First Name: $firstName")
        println("Last Name: $lastName")
        println("verified: $verified")
    }//debug meathod for printing

    fun verify(){
        verified = true
    }//verify account

    fun get_verify(): Boolean {
        return verified
    }//get the account verification

}