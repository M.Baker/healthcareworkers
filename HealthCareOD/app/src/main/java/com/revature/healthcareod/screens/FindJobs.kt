/*
Healthcare OnDemand
Anderson Adams
Find Jobs Class
Class will create/display job finder recycle adapter
 */

package com.revature.healthcareod.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentFindJobsBinding
import com.revature.healthcareod.datahandlers.*

class FindJobs : Fragment() {

    private var _binding: FragmentFindJobsBinding?=null
    private val binding get()=_binding!!
    lateinit var userName:String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        println("The view is being created")
        super.onCreate(savedInstanceState)

        arguments?.let{
            println("user name being assigned")
            userName=it.getString("user").toString()
        }
        if(userName!=null){
            println("user being assigned")
            userRef= Users.getUser(userName)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding= FragmentFindJobsBinding.inflate(inflater,container,false)
        val view=binding.root
        //bind adapter with all open jobs list
        binding.rvFindJobs.adapter= JobFinder(this.requireContext(),userRef!!.getAllOpenJobs(), userName)
        binding.btnFjBack.setOnClickListener { back() }


        // binding.btnBack.setOnClickListener { back() }
        //binding.btnSaveProfile.setOnClickListener { submit() }
        //Inflate the layout for this fragment
        return view
    }


    private fun back(){
        findNavController().navigate(
            FindJobsDirections.actionFindJobsToJobSearcher(
                user = userName
            )
        )
    }
}